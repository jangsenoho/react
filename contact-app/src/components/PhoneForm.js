import React, { Component } from 'react';

class PhoneForm extends Component {

  state = {
    name:'',
    phone:'',
  }

  handelChange = (e) =>{
    this.setState({
     [e.target.name]: e.target.value
    });
  }
  handelSubmit = (e) =>{
    e.preventDefault();
    this.props.onCreate(this.state);
    this.setState({
      name:'',
      phone:'',
    })

  }
  render() {
    return (
      <div>
        <form onSubmit={this.handelSubmit}>
          <input 
            name="name"
            placeholder="이름"
            onChange={this.handelChange}
            value={this.state.name}
            />
            <input 
            name="phone"
            placeholder="전화번호"
            onChange={this.handelChange}
            value={this.state.phone}
            />
            <button type="submit">등록</button>
        </form>
      </div>
    );
  }
}

export default PhoneForm;