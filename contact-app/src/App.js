import React, { Component } from 'react';
import PhoneForm from './components/PhoneForm';
import PhoneInfoList from './components/PhoneInfoList';

class App extends Component {

  id= 0; 
  state = {
    information:[],
  }

  handleCreate = (data) =>{
    const {information} = this.state;
    this.setState({ 
      information: information.concat(Object.assign({}, data,{
        id:this.id++
      }))
    });
  }

  handleUpdate = (id, data) => {
    const { information } = this.state;
    this.setState({
      information: information.map(info => {
        if (info.id === id) {
          return {
            id,
            ...data
          };
        }
        return info;
      })
    });
  };
  
  render() {
    return (
      <div>
        <PhoneForm  onCreate={this.handleCreate}/>
       <PhoneInfoList
        data={this.state.information}
        onRemove={this.handleRemove}
       
       />
      </div>
    );
  }
}

export default App;